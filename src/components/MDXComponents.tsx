import React from "react";
import {
  chakra,
  Divider,
  Heading,
  OrderedList,
  Text,
  UnorderedList,
  ListItem,
  useColorMode,
  useColorModeValue,
} from "@chakra-ui/react";
import Image from "next/image";
import {
  lightCodebgColor,
  lightCodeColor,
  darkCodeColor,
  darkCodebgColor,
  monofont,
  lightBorderColor,
  darkBorderColor,
} from "@/config/sitestyles";
import { ChakraProps, HeadingProps, HTMLChakraProps } from "@chakra-ui/react";
import { ImageProps } from "next/image";

type CustomImageProps = Omit<ImageProps, "src"> & {
  src: string;
  alt: string;
};

const CustomImage: React.FC<CustomImageProps> = ({ src, alt, ...rest }) => {
  return (
    <Image
      width={300}
      height={300}
      sizes="(max-width: 768px) 60vw"
      src={src}
      alt={alt}
      {...rest}
    />
  );
};

const Quote: React.FC<ChakraProps> = (props) => {
  return (
    <chakra.blockquote
      mt={3}
      pl="20px"
      marginInlineStart="20px"
      borderLeftWidth="2px"
      borderLeftColor={useColorModeValue(lightBorderColor, darkBorderColor)}
      bg="transparent"
      {...props}
    />
  );
};

const Hr: React.FC = () => {
  const { colorMode } = useColorMode();
  const borderColor = {
    light: lightBorderColor,
    dark: darkBorderColor,
  };

  return <Divider borderColor={borderColor[colorMode]} my={4} w="100%" />;
};

const InlineCode: React.FC<ChakraProps> = (props) => (
  <chakra.code
    apply="mdx.code"
    fontFamily="Menlo, Consolas, Monaco, Liberation Mono, Lucida Console, monospace"
    color={useColorModeValue(lightCodeColor, darkCodeColor)}
    bg={useColorModeValue(lightCodebgColor, darkCodebgColor)}
    px={1}
    py={0.5}
    rounded={{ base: "md", md: "lg" }}
    {...props}
  />
);

const MDXCode: React.FC<ChakraProps> = (props) => {
  return (
    <chakra.pre
      fontFamily={monofont}
      overflowY="hidden"
      overflowX="auto"
      rounded="md"
      p={3}
      borderWidth="1px"
      mt={4}
      mb={4}
      borderColor={useColorModeValue(lightBorderColor, darkBorderColor)}
    >
      <chakra.span {...props} />
    </chakra.pre>
  );
};

const MDXComponents = {
  code: InlineCode,
  pre: MDXCode,
  h1: (props: HeadingProps) => (
    <Heading
      as="h1"
      fontSize="3xl"
      pb={0.5}
      fontWeight="normal"
      borderBottomWidth="1px"
      mb={3}
      mt={3}
      {...props}
    />
  ),
  h2: (props: HeadingProps) => (
    <Heading
      as="h2"
      fontSize="2xl"
      pb={0.5}
      fontWeight="normal"
      borderBottomWidth="1px"
      mb={3}
      mt={3}
      {...props}
    />
  ),
  h3: (props: HeadingProps) => (
    <Heading
      as="h3"
      fontSize="xl"
      pb={0.5}
      fontWeight="normal"
      borderBottomWidth="1px"
      mb={3}
      mt={3}
      {...props}
    />
  ),
  h4: (props: HeadingProps) => (
    <Heading
      as="h4"
      fontSize="lg"
      {...props}
      pb={0.5}
      fontWeight="normal"
      borderBottomWidth="1px"
      mb={2}
      mt={2}
    />
  ),
  hr: Hr,
  p: (props: HTMLChakraProps<"p">) => (
    <Text mt={2} mb={4} lineHeight={1.5} letterSpacing={1} {...props} />
  ),
  ul: (props: HTMLChakraProps<"ul">) => (
    <UnorderedList as="ul" pt={2} pl={4} mt={4} mb={4} {...props} />
  ),
  ol: (props: HTMLChakraProps<"ol">) => (
    <OrderedList as="ol" pt={2} pl={6} mt={4} mb={4} {...props} />
  ),
  li: (props: HTMLChakraProps<"li">) => <ListItem as="li" {...props} />,
  blockquote: Quote,
  img: CustomImage as React.ComponentType<React.HTMLProps<HTMLImageElement>>,
};

export default MDXComponents;
