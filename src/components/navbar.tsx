import { Link } from "@chakra-ui/next-js";
import {
  Container,
  Box,
  Menu,
  MenuItem,
  MenuList,
  MenuButton,
  IconButton,
  Text,
  useBreakpointValue,
  Flex,
  useColorModeValue,
} from "@chakra-ui/react";
import { HamburgerIcon, SearchIcon, ChevronLeftIcon } from "@chakra-ui/icons";
import React, { ReactElement, ReactNode } from "react";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import {
  lightMenuColor,
  darkMenuColor,
  lightMenubgColor,
  darkMenubgColor,
  lightPCMenubgColor,
  darkPCMenubgColor,
  lightNavColor,
  darkNavColor,
  lightmainColor,
  darkmainColor,
} from "@/config/sitestyles";

type LinkProps = {
  children: ReactNode;
  href: string;
};
const LinkItem = ({ href, children }: LinkProps): ReactElement => {
  const router = useRouter();
  const path = router.asPath;
  const active = path === href;
  const LinkColor = useColorModeValue(lightMenuColor, darkMenuColor);
  const bgColor = useColorModeValue(lightPCMenubgColor, darkPCMenubgColor);
  return (
    <Link
      href={href}
      scroll={false}
      p={2}
      color={LinkColor}
      bg={active ? bgColor : undefined}
      borderRadius="md"
    >
      {children}
    </Link>
  );
};
const MenuLink = ({ href, children }: LinkProps): ReactElement => {
  const router = useRouter();
  const path = router.asPath;
  const active = path === href;
  const LinkColor = useColorModeValue(lightMenuColor, darkMenuColor);
  const bgColor = useColorModeValue(lightMenubgColor, darkMenubgColor);
  return (
    <Link href={href}>
      <MenuItem color={LinkColor} bg={active ? bgColor : "transparent"}>
        {children}
      </MenuItem>
    </Link>
  );
};
const Navbar = ({ children }: { children: React.ReactNode }) => {
  const router = useRouter();
  const isSearchPage = router.asPath === "/search";
  const isAboutPage = router.asPath === "/about";
  const isMobileView = useBreakpointValue({ base: true, md: false });
  const isComputerView = useBreakpointValue({ base: false, md: true });

  const [scrollPosition, setScrollPosition] = useState(0);
  const [interpolatedColor, setInterpolatedColor] = useState("");

  useEffect(() => {
    const handleScroll = () => {
      const currentPosition = window.scrollY;
      setScrollPosition(currentPosition);
    };

    window.addEventListener("scroll", handleScroll);
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  const initialColor = useColorModeValue(lightNavColor, darkNavColor);
  const targetColor = useColorModeValue(lightmainColor, darkmainColor);

  useEffect(() => {
    const calculateInterpolatedColor = (
      startColor: string,
      endColor: string,
      progress: number,
    ) => {
      const startR = parseInt(startColor.slice(1, 3), 16);
      const startG = parseInt(startColor.slice(3, 5), 16);
      const startB = parseInt(startColor.slice(5, 7), 16);

      const endR = parseInt(endColor.slice(1, 3), 16);
      const endG = parseInt(endColor.slice(3, 5), 16);
      const endB = parseInt(endColor.slice(5, 7), 16);

      const interpolatedR = Math.floor(startR + (endR - startR) * progress);
      const interpolatedG = Math.floor(startG + (endG - startG) * progress);
      const interpolatedB = Math.floor(startB + (endB - startB) * progress);

      return `rgb(${interpolatedR}, ${interpolatedG}, ${interpolatedB})`;
    };

    const progress = Math.min(1, scrollPosition / 50); // Adjust the divisor as needed

    const interpolatedColor = calculateInterpolatedColor(
      initialColor,
      targetColor,
      progress,
    );

    setInterpolatedColor(interpolatedColor);
  }, [scrollPosition, initialColor, targetColor]); // Add initialColor and targetColor here

  return (
    <Box
      position="fixed"
      as="nav"
      w="100%"
      bg={scrollPosition > 0 ? interpolatedColor : initialColor}
      transition="background 0.3s"
      zIndex={1}
    >
      <Container p={2} maxW="container.lg">
        <Flex alignItems="center">
          {isMobileView && !isSearchPage && !isAboutPage && (
            <Menu autoSelect={false}>
              <MenuButton
                as={IconButton}
                aria-label="Options"
                icon={<HamburgerIcon />}
                variant="ghost"
                aria-controls="none"
              />
              <MenuList>
                <MenuLink href="/">主页</MenuLink>
                <MenuLink href="/posts">文章</MenuLink>
                <MenuLink href="/about">关于</MenuLink>
              </MenuList>
            </Menu>
          )}

          {(isSearchPage || isAboutPage) && isMobileView && (
            <Link href="/">
              <IconButton
                aria-label="Back"
                icon={<ChevronLeftIcon />}
                variant="ghost"
                onClick={() => router.back()}
              />
            </Link>
          )}
          <Text
            fontWeight="bold"
            fontSize="lg"
            ml={2}
            display={isMobileView ? "block" : "none"}
          >
            {children}
          </Text>

          {isMobileView && !isSearchPage && (
            <Box flex={1} textAlign="right">
              <Link href="/search">
                <IconButton
                  aria-label="Search database"
                  icon={<SearchIcon />}
                  variant="ghost"
                />
              </Link>
            </Box>
          )}
          {isComputerView && (
            <Box mt={4}>
              <LinkItem href="/">主页</LinkItem>{" "}
              <LinkItem href="/posts">文章</LinkItem>
              <LinkItem href="/about">关于</LinkItem>
              <LinkItem href="/search">搜索</LinkItem>
            </Box>
          )}
        </Flex>
      </Container>
    </Box>
  );
};

export default Navbar;
