import { extendTheme } from "@chakra-ui/react";
import { mode } from "@chakra-ui/theme-tools";
import type { StyleFunctionProps } from "@chakra-ui/styled-system";
import {
  lightbgColor,
  darkbgColor,
  lightColor,
  darkColor,
  font,
  lightLinkColor,
  darkLinkColor,
} from "@/config/sitestyles";

const styles = {
  global: (props: StyleFunctionProps) => ({
    body: {
      color: mode(lightColor, darkColor)(props),
      bg: mode(lightbgColor, darkbgColor)(props),
      fontFamily: font,
      lineHeight: "base",
    },
  }),
};

const components = {
  Link: {
    basestyle: (props: StyleFunctionProps) => ({
      color: mode(lightLinkColor, darkLinkColor)(props),
      textUnderlinOffset: 3,
    }),
  },
};

const config = {
  initialColorMode: "system",
  useSystemColorMode: true,
};
const fonts = {
  heading: font,
};
const theme = extendTheme({
  config,
  components,
  styles,
  fonts,
});

export default theme;
