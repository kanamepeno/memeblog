import React, { ReactNode } from "react";
import { GetStaticProps } from "next";
import { getPostsFrontMatter, PartialFrontMatter } from "@/lib/get-posts";
import Layout from "@/components/layouts";
import { Tag, TagLeftIcon, TagLabel, Box, Grid } from "@chakra-ui/react";
import { AddIcon } from "@chakra-ui/icons";
import { Link } from "@chakra-ui/next-js";
import PostsList from "@/components/PostsList";

type PostsProps = {
  Posts: PartialFrontMatter[];
};

const Posts = ({ Posts }: PostsProps): ReactNode => {
  const filteredBlogPosts = Posts.sort(
    (a, b) => Number(new Date(b.date)) - Number(new Date(a.date)),
  );

  return (
    <Layout title="文章">
      <Box p={2}>
        <Link href="/tags">
          <Tag size="md" colorScheme="gray" mb={10}>
            <TagLeftIcon boxSize="12px" as={AddIcon} />
            <TagLabel>All tags</TagLabel>
          </Tag>
        </Link>
        <Grid
          templateColumns={{ md: "repeat(2, 1fr)", base: "repeat(1, 1fr)" }}
          gap={4}
        >
          <PostsList posts={filteredBlogPosts} />
        </Grid>
      </Box>
    </Layout>
  );
};

export const getStaticProps: GetStaticProps = () => {
  const Posts = getPostsFrontMatter();
  return {
    props: {
      Posts,
    },
  };
};

export default Posts;
