import React, { ReactNode } from "react";
import Layout from "@/components/layouts";
import { ExternalLinkIcon } from "@chakra-ui/icons";
import { FaTelegram, FaHeartbeat } from "react-icons/fa";
import { AiOutlineAndroid } from "react-icons/ai";
import { SiMaildotru, SiNixos, SiArchlinux } from "react-icons/si";
import { GetStaticProps } from "next";
import { getPostsFrontMatter } from "@/lib/get-posts";
import { generateMainFeeds } from "@/lib/feeds";
import {
  Box,
  Stat,
  StatLabel,
  StatNumber,
  StatHelpText,
  Button,
  Text,
  Code,
  List,
  ListItem,
  Icon,
  Tab,
  Tabs,
  TabList,
  TabPanel,
  TabPanels,
  Tag,
  TagLabel,
  TagRightIcon,
} from "@chakra-ui/react";
import { Link } from "@chakra-ui/next-js";
import Image from "next/image";

const Home = (): ReactNode => {
  return (
    <Layout title="主页">
      <Box p={2}>
        <Box fontFamily="Menlo, Consolas, Monaco, Liberation Mono, Lucida Console, monospace">
          <Stat mt={16} mb={2}>
            <StatLabel>Who am I</StatLabel>
            <StatNumber>ITY</StatNumber>
            <StatHelpText>( Developer / Designer )</StatHelpText>
          </Stat>
          <Link href="mailto:ity@keemail.me" aria-label="Email">
            <Icon as={SiMaildotru} w={6} h={6} mr={3} />
          </Link>
          <Link href="https://t.me/williexu" aria-label="Telegram">
            <Icon as={FaTelegram} w={6} h={6} mr={1} />
          </Link>
          <Text my={4}>
            我是一个来自中国的大学生。我是一个电脑相关技术爱好者，喜欢探索未知的领域，喜欢极简主义和自由主义。当我在小学的时候，我就会给当时的安卓手机刷机，这给了我继续探索电脑相关技术的动力。在我把一台小米6x刷成类原生后，我喜欢上了极简的风格。所以当我去建立网站时，我会选择贴近谷歌的设计以及保持极简风格。梦想成为一名黑客，但是我在数学和计算机原理领域中的知识还不足以实现这个梦想。但是我会一直向前走！
          </Text>
          <Text my={2}>
            直到现在，我一直都支持自由软件主义。我希望我周围的软件都是自由的。现在我常用软件（包括操作系统）有
            Linux, LineageOS, NeoVim, IRC, Matrix...
          </Text>
          <Link href="/posts" mr="2rem">
            <Button colorScheme="gray">
              博客
              <ExternalLinkIcon mx="2px" />
            </Button>
          </Link>
          <Link href="https://sr.ht/~ity/" mr="2px">
            <Button colorScheme="gray">
              SourceHut
              <ExternalLinkIcon mx="2px" />
            </Button>
          </Link>
          <Box mt={5}>
            <Tabs>
              <TabList>
                <Tab>OS</Tab> <Tab>Languages</Tab> <Tab>Like</Tab>
              </TabList>
              <TabPanels>
                <TabPanel>
                  <List>
                    <ListItem>
                      <Tag size="md" my={3} mr={2}>
                        <TagLabel>Android</TagLabel>
                        <TagRightIcon as={AiOutlineAndroid} />
                      </Tag>
                    </ListItem>
                    <ListItem>
                      <Tag size="md" my={3} mr={2}>
                        <TagLabel>Arch Linux</TagLabel>
                        <TagRightIcon as={SiArchlinux} />
                      </Tag>
                    </ListItem>
                    <ListItem>
                      <Tag size="md" my={3} mr={2}>
                        <TagLabel>NixOS</TagLabel> <TagRightIcon as={SiNixos} />
                      </Tag>
                    </ListItem>
                  </List>
                </TabPanel>
                <TabPanel>
                  <List>
                    <ListItem mt={3}>
                      <Code>Javascript</Code>
                    </ListItem>
                    <ListItem mt={3}>
                      <Code>ShellScript</Code>
                    </ListItem>
                    <ListItem mt={3}>
                      <Code>React</Code>
                    </ListItem>
                    <ListItem mt={3}>
                      <Code>Python</Code>
                    </ListItem>
                  </List>
                </TabPanel>
                <TabPanel>
                  <Tag size="md" variant="outline" my={3}>
                    <TagLabel>Kano</TagLabel> <TagRightIcon as={FaHeartbeat} />
                  </Tag>
                  <Image
                    src="/images/kano-1.webp"
                    width={300}
                    height={300}
                    alt="logo"
                    loading="lazy"
                  />
                </TabPanel>
              </TabPanels>
            </Tabs>
          </Box>
        </Box>
      </Box>
    </Layout>
  );
};

export const getStaticProps: GetStaticProps = () => {
  generateMainFeeds();
  const allPosts = getPostsFrontMatter();
  return { props: { allPosts } };
};

export default Home;
