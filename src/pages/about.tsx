import React, { ReactNode } from "react";
import Layout from "@/components/layouts";
import {
  Box,
  Button,
  Text,
  Icon,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalBody,
  ModalFooter,
  useColorModeValue,
  Stack,
  useColorMode,
  Avatar,
  useDisclosure,
} from "@chakra-ui/react";
import {
  LinkIcon,
  ViewIcon,
  InfoIcon,
  WarningIcon,
  MoonIcon,
  SunIcon,
} from "@chakra-ui/icons";
import { Link } from "@chakra-ui/next-js";
import { GetStaticProps } from "next";
import friendsData from "@/config/friends.json";
import {
  siteTitle,
  siteLogo,
  darkLogo,
  Licence,
  version,
  sourceUrl,
} from "@/config/index";

type aboutProps = {
  friends: Array<{
    link: string;
    avatar: string;
    title: string;
    description: string;
    rss: string;
  }>;
};
const About = ({ friends }: aboutProps): ReactNode => {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const { colorMode, toggleColorMode } = useColorMode();

  return (
    <Layout title="关于本站">
      <Box p={2}>
        <Box
          p={2}
          mt={16}
          borderWidth="1px"
          borderColor={useColorModeValue("#DDD", "#808A8F50")}
          rounded="md"
          fontFamily="Menlo, Consolas, Monaco, Liberation Mono, Lucida Console, monospace"
        >
          <Stack direction="row" alignItems="center">
            <Avatar src={useColorModeValue(siteLogo, darkLogo)} />
            <Text ml={2} fontSize="x-large">
              {siteTitle}
            </Text>
          </Stack>

          <Button
            variant="ghost"
            width="full"
            mt={4}
            fontSize="lg"
            display="flex"
            justifyContent="flex-start"
            alignItems="center"
          >
            <Icon as={InfoIcon} mr={8} />
            <Box textAlign="left">
              <Text>版本</Text>
              <Text>{version}</Text>
            </Box>
          </Button>
          <Link href={sourceUrl}>
            <Button
              variant="ghost"
              width="full"
              mt={4}
              fontSize="lg"
              display="flex"
              justifyContent="flex-start"
              alignItems="center"
            >
              <Icon as={ViewIcon} mr={8} />
              <Text>源码</Text>
            </Button>
          </Link>
          <Button
            variant="ghost"
            width="full"
            onClick={onOpen}
            mt={4}
            fontSize="lg"
            display="flex"
            justifyContent="flex-start"
            alignItems="center"
          >
            <Icon as={LinkIcon} mr={8} />
            <Text>友情链接</Text>
          </Button>
          <Button
            variant="ghost"
            width="full"
            mt={4}
            fontSize="lg"
            display="flex"
            justifyContent="flex-start"
            alignItems="center"
          >
            <Icon as={WarningIcon} mr={8} />
            <Text>{Licence}</Text>
          </Button>
          <Modal
            isCentered
            onClose={onClose}
            isOpen={isOpen}
            motionPreset="slideInBottom"
            size="sm"
          >
            <ModalOverlay />
            <ModalContent>
              <ModalHeader>列表</ModalHeader>
              <ModalBody>
                {friends.map((friend, index) => (
                  <Box key={index}>
                    <Link href={friend.link}>
                      <Box p={2} rounded="md" my={6}>
                        <Stack direction="row" alignItems="center" mb={2}>
                          <Avatar src={friend.avatar} name={friend.title} />
                          <Text fontSize="x-large">{friend.title}</Text>
                        </Stack>

                        {friend.description && (
                          <Text mb={2}>{friend.description}</Text>
                        )}
                        {friend.rss && <Link href={friend.rss}>订阅RSS</Link>}
                      </Box>
                    </Link>
                  </Box>
                ))}
              </ModalBody>
              <ModalFooter>
                <Button mr={3} onClick={onClose}>
                  Close
                </Button>
              </ModalFooter>
            </ModalContent>
          </Modal>
          <Button
            variant="ghost"
            width="full"
            onClick={toggleColorMode}
            mt={4}
            fontSize="lg"
            display="flex"
            justifyContent="flex-start"
            alignItems="center"
          >
            <Icon as={useColorModeValue(SunIcon, MoonIcon)} mr={8} />
            <Text> {colorMode === "light" ? "Light Theme" : "Dark Theme"}</Text>
          </Button>
        </Box>
      </Box>
    </Layout>
  );
};

export const getStaticProps: GetStaticProps = async () => {
  return {
    props: {
      friends: friendsData,
    },
  };
};

export default About;
