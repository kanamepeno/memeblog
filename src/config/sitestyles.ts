// 主要字体
export const font = `ui-sans-serif, system-ui`;

// 代码字体
export const monofont = `Menlo, Consolas, Monaco, Liberation Mono, Lucida Console, monospace`;

// 亮色模式网页字体颜色
export const lightColor = `#333`;

// 暗色模式网页字体颜色
export const darkColor = `#E2E8F0`;

// 亮色模式网页背景颜色
export const lightbgColor = `#FCFCFC`;

// 暗色模式网页背景颜色
export const darkbgColor = `#1B1B1B`;

// 亮色模式链接字体颜色
export const lightLinkColor = `#3D7AED`;

// 暗色模式链接字体颜色
export const darkLinkColor = `#FF63C3`;

// 亮色模式边框线条颜色
export const lightBorderColor = `#DDD`;

// 暗色模式边框线条颜色
export const darkBorderColor = `#808A8F50`;

// 亮色模式文章按钮背景颜色
export const lightPostButtonColor = `#d0dce74d`;

// 暗色模式文章按钮背景颜色
export const darkPostButtonColor = `#ffffff14`;

// 亮色模式菜单链接颜色
export const lightMenuColor = `gray.800`;

// 暗色模式菜单链接颜色
export const darkMenuColor = `whiteAlpha.900`;

// 亮色模式电脑界面菜单背景颜色
export const lightPCMenubgColor = `#d0dce74d`;

// 暗色模式电脑界面菜单背景颜色
export const darkPCMenubgColor = `#38383880`;

// 亮色模式手机界面菜单背景颜色
export const lightMenubgColor = `#d0dce74d`;

// 暗色模式手机界面菜单背景颜色
export const darkMenubgColor = `#85858580`;

// 亮色模式标题栏最终颜色和主题颜色
export const lightmainColor = `#D0DCE74D`;

// 亮色模式初始标题栏颜色
export const lightNavColor = `#FCFCFC`;

// 暗色模式标题栏最终颜色和主题颜色
export const darkmainColor = `#20202380`;

// 暗色模式初始标题栏颜色
export const darkNavColor = `#1B1B1B`;

// 亮色模式代码字体颜色
export const lightCodeColor = `gray.600`;

// 暗色模式代码字体颜色
export const darkCodeColor = `gray.300`;

// 亮色模式代码背景颜色
export const lightCodebgColor = `gray.50`;

// 暗色模式代码背景颜色
export const darkCodebgColor = `whiteAlpha.200`;
