// 网站域名
export const domain = `ity.moe`;

// 网站语言
export const language = `zh`;

// 作者
export const author = `ity`;

// 网站标题
export const siteTitle = `ITY Space`;

// 网站描述
export const siteDescription = `ITY Space`;

// 网站 Logo
export const siteLogo = `/icon.png`;

// 暗色版网站 Logo
export const darkLogo = `/darkicon.png`;

// 网站许可证
export const Licence = `CC BY-NC-SA 4.0`;

// 网站源码地址
export const sourceUrl = `https://gitlab.com/itys/memeblog`;

// 网站版本号
export const version = `1.0.0`;

export const copyright = `Copyright ${new Date().getFullYear()} - ${author}`;

export const baseUrl = `https://${domain}`;
