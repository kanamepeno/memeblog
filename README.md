# Welcome to MemeBlog!

## 开始使用

```Shell
git clone https://git.sr.ht/~ity/MemeBlog
npm i
npm run dev
```

### 可以在[vercel](https://vercel.com)云部署

## 使用提示

- Markdown 里只有四种媒体信息: title, description, tags, date。tags 里的内容要用[]包住。

- 如果自用，请修改 源码中所有涉及到我的信息

## 更新日志

[v1.0.0] 修复bugs
